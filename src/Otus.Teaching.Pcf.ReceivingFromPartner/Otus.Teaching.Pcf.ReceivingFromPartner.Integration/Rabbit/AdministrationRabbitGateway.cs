using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Rabbit
{
    public class AdministrationRabbitGateway : IAdministrationGateway
    {
        private readonly IPublishEndpoint _endpoint;

        public AdministrationRabbitGateway(IPublishEndpoint endpoint)
        {
            _endpoint = endpoint;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var message = new NotifyAdminAboutPartnerManagerPromoCodeMessage
            {
                ManagerIdPartnerManagerId = partnerManagerId
            };
            await _endpoint.Publish(message);
        }
    }
}
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Rabbit
{
    public class GivingPromoCodeToCustomerRabbitGateway : IGivingPromoCodeToCustomerGateway
    {
        private readonly IPublishEndpoint _endpoint;
        
        public GivingPromoCodeToCustomerRabbitGateway(IPublishEndpoint endpoint)
        {
            _endpoint = endpoint;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var message = new GivePromoCodeToCustomerMessage
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            await _endpoint.Publish(message);
        }
    }
}
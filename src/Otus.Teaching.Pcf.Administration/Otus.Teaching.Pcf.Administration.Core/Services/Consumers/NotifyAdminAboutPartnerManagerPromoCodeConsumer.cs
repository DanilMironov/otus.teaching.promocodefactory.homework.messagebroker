using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Messages;

namespace Otus.Teaching.Pcf.Administration.Core.Services.Consumers
{
    public class NotifyAdminAboutPartnerManagerPromoCodeConsumer : IConsumer<NotifyAdminAboutPartnerManagerPromoCodeMessage>
    {
        private readonly IEmployeeService _employeeService;
        
        public NotifyAdminAboutPartnerManagerPromoCodeConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        public async Task Consume(ConsumeContext<NotifyAdminAboutPartnerManagerPromoCodeMessage> context)
        {
            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.ManagerIdPartnerManagerId);
        }
    }
}
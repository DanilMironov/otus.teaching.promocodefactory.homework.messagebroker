using System;

namespace Otus.Teaching.Pcf.Administration.Core.Messages
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMessage
    {
        public Guid ManagerIdPartnerManagerId { get; set; } 
    }
}
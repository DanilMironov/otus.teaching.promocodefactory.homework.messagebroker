namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    /// <summary>
    /// Настройки подключения к RabbitMq
    /// </summary>
    public class RabbitSettings
    {
        public string Hostname { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerMessage>
    {
        private readonly IPromoCodeService _promocodeService;
        
        public GivePromoCodeToCustomerConsumer(IPromoCodeService promocodeService)
        {
            _promocodeService = promocodeService;
        }
        
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            await _promocodeService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
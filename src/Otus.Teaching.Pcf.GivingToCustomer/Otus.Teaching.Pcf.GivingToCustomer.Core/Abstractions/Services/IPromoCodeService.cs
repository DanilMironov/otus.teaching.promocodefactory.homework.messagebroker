using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerMessage request);
    }
}